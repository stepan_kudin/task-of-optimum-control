#include <iostream>

#include "const.h"
#include "lib.h"


int main()
{
  // Ввод данных
  double t, tau, n;
  // Параметр
  std::cout << "T = ";
  std::cin >> t;
  std::cout << "";

  // Количество шагов для алгоритма Рунге-Кутта
  std::cout << "n = ";
  std::cin >> n;

  // Размер шага
  tau = t / n;

  // Численное решение
  // Нахождение недостающих начальных условий

  // Начальное приближение
  double boundary_condition_1 = 0.1;
  double boundary_condition_2 = 0.0;

  // Алгоритмом Рунге-Кутты решаем задачу
  point boundary_condition;
  boundary_condition.x_1 = 0.0;
  boundary_condition.x_2 = boundary_condition_1;
  boundary_condition.p_1 = boundary_condition_2;
  boundary_condition.p_2 = 0.0;

  point ret = runge_kutta(boundary_condition, tau, n, false);

//  std::cout << ret.x_1 << " " << ret.x_2 << std::endl;

  // Считаем вектор ошибки и невязку
  double error_vector[2];
  error_vector[0] = ret.x_1;
  error_vector[1] = ret.x_2 - 1.0;

  double residual = norm(error_vector[0], error_vector[1]);

  // Проверяем, маленькая невязка или нет
  bool newton_flag;
  if(residual < EPS)
    newton_flag = false;
  else
    newton_flag = true;

  // Если нет применяем алгоритм Ньютона
  if(newton_flag)
    ret = newton_solver(boundary_condition, tau, n);

  point ret_2 = runge_kutta(ret, tau, n, true);

  // Методом Симпсона считаем значение функционала
  double result = integration_functions(ret, t, n);

  std::cout << "tau = " << tau << " h_sim = " << tau << " x_2(0) = " << ret.x_2 << " p_1(0) = " << ret.p_1 << std::endl;
  std::cout << "result = " << result << std::endl;

  // Нахождение значения функционала
  // Выдача ответа
  return 0;
}

