#include <iostream>
#include <math.h>

#include "types.h"
#include "operators.h"
#include "lib.h"

// Алгоритм Рунге-Кутта
point runge_kutta(point starting_condition, double tau, int n, bool flag_print)
{
  int index;
  point k_1, k_2, k_3, k_4, answer;
  FILE *out;
  double t = 0.0;


  if(flag_print)
    out = fopen("./u.gnuplot", "w");

  answer = starting_condition;

  for(index = 0; index < n; ++index)
    {
      if(flag_print)
        fprintf(out, "%lf %lf\n", answer.p_2, t);
      k_1 = f(answer) * tau;
      k_2 = f(answer + 0.5 * k_1) * tau;
      k_3 = f(answer + 0.5 * k_2) * tau;
      k_4 = f(answer + k_3) * tau;
      answer = answer + (k_1 + 2 * k_2 + 2 * k_3 + k_4) / 6;
      t += tau;

//      std::cout << answer.p_2 << " " << t << std::endl;
      if(flag_print)
        fprintf(out, "%lf %lf\n\n\n", answer.p_2, t);
    }

  if(flag_print)
    fclose(out);

  return answer;
}

point f(point u)
{
  point ret;

  ret.x_1 = u.x_2;
  ret.x_2 = u.p_2;
  ret.p_1 = -u.x_1;
  ret.p_2 = -u.p_1;
  return ret;
}

double integrand(point value)
{
  return value.p_2 * value.p_2 - value.x_1 * value.x_1;
}

double simpson_rule(point point_1, point point_2, point point_3, double tau)
{
  return (tau / 3) * (integrand(point_1) + 4 * integrand(point_2) + integrand(point_3));
}

double integration_functions(point starting_condition, double t, int count_steps)
{
  point ret_1, ret_2, start;
  double tau = t / (2 * count_steps);
  double sum = 0.0;

  start = starting_condition;

  for(int index = 0; index < count_steps; ++index)
    {
      ret_1 = runge_kutta(start, tau, 1, false);
      ret_2 = runge_kutta(ret_1, tau, 1, false);
      sum += simpson_rule(start, ret_1, ret_2, tau);
      start = ret_2;
    }

  return sum;
}

double norm(double x, double y)
{
  return sqrt(x * x + y * y);
}

point newton_solver(point boundary_condition, double tau, int n)
{
  double gamma, det, a_1, a_2, next_a_1, next_a_2, x_11, x_12, x_21, x_22, k_1, k_2;
  double error, next_error;
  int counter = 0;
  point ret_1, ret_2, starting_condition;

  a_1 = boundary_condition.x_2;
  a_2 = boundary_condition.p_1;

  starting_condition.x_1 = boundary_condition.x_1;
  starting_condition.p_2 = boundary_condition.p_2;

  while(counter < MAX_STEPS)
    {
      // Инициализация итерации
      gamma = 1.0;

      // Найдём элементы матрицы Якоби
      starting_condition.x_2 = a_1;
      starting_condition.p_1 = a_2;

      ret_1 = runge_kutta(starting_condition, tau, n, false);

      starting_condition.x_2 = a_1 + DELTA;
      starting_condition.p_1 = a_2;

      ret_2 = runge_kutta(starting_condition, tau, n, false);

      x_11 = (ret_2.x_1 - ret_1.x_1) / DELTA;
      x_21 = (ret_2.x_2 - ret_1.x_2) / DELTA;

      starting_condition.x_2 = a_1;
      starting_condition.p_1 = a_2 + DELTA;

      ret_2 = runge_kutta(starting_condition, tau, n, false);

      x_12 = (ret_2.x_1 - ret_1.x_1) / DELTA;
      x_22 = (ret_2.x_2 - ret_1.x_2) / DELTA;

      // Считаем невязку с нормировкой Федоренко
      k_1 = x_11 * x_11 + x_12 * x_12;
      k_2 = x_21 * x_21 + x_22 * x_22;

      error = sqrt(ret_1.x_1 * ret_1.x_1 / k_1 + (ret_1.x_2 - 1) * (ret_1.x_2 - 1) / k_2);

    // Если норма достаточно маленькая, заканчиваем алгоритм
      if(error < EPS)
        return ret_1;

      next_error = 1;

      // Если нет, ищем начальные условия так, чтобы норма уменьшилась относительно предыдущего шага
      while(next_error > error)
        {
          det = x_11 * x_22 - x_12 * x_21;

          next_a_1 = a_1 - gamma * (ret_1.x_1 * x_22 - (ret_1.x_2 - 1) * x_12) / det;
          next_a_2 = a_2 - gamma * ((ret_1.x_2 - 1) * x_11 - ret_1.x_1 * x_21) / det;

          gamma = gamma / 2;

          starting_condition.x_2 = next_a_1;
          starting_condition.p_1 = next_a_2;

          ret_1 = runge_kutta(starting_condition, tau, n, false);

          starting_condition.x_2 = next_a_1 + DELTA;
          starting_condition.p_1 = next_a_2;

          ret_2 = runge_kutta(starting_condition, tau, n, false);

          x_11 = (ret_2.x_1 - ret_1.x_1) / DELTA;
          x_21 = (ret_2.x_2 - ret_1.x_2) / DELTA;

          starting_condition.x_2 = next_a_1;
          starting_condition.p_1 = next_a_2 + DELTA;

          ret_2 = runge_kutta(starting_condition, tau, n, false);

          x_12 = (ret_2.x_1 - ret_1.x_1) / DELTA;
          x_22 = (ret_2.x_2 - ret_1.x_2) / DELTA;

          // Считаем невязку с нормировкой Федоренко
          k_1 = x_11 * x_11 + x_12 * x_12;
          k_2 = x_21 * x_21 + x_22 * x_22;

          next_error = sqrt(ret_1.x_1 * ret_1.x_1 / k_1 + (ret_1.x_2 - 1) * (ret_1.x_2 - 1) / k_2);

          if(next_error < EPS)
            {
              starting_condition.x_2 = next_a_1;
              starting_condition.p_1 = next_a_2;
              return starting_condition;
            }
        }
      // Заканчиваем итерацию
      a_1 = next_a_1;
      a_2 = next_a_2;

      error = next_error;
      ++counter;

    }

  starting_condition.x_2 = a_1;
  starting_condition.p_1 = a_2;

  return starting_condition;
}


