#include "operators.h"

point operator +(point left, point right)
{
  point ret;

  ret.x_1 = left.x_1 + right.x_1;
  ret.x_2 = left.x_2 + right.x_2;
  ret.p_1 = left.p_1 + right.p_1;
  ret.p_2 = left.p_2 + right.p_2;

  return ret;
}

point operator *(double number, point vector)
{
  point ret;

  ret.x_1 = number * vector.x_1;
  ret.x_2 = number * vector.x_2;
  ret.p_1 = number * vector.p_1;
  ret.p_2 = number * vector.p_2;

  return ret;
}

point operator *(point vector, double number)
{
  point ret;

  ret.x_1 = number * vector.x_1;
  ret.x_2 = number * vector.x_2;
  ret.p_1 = number * vector.p_1;
  ret.p_2 = number * vector.p_2;

  return ret;
}

point operator /(point vector, double number)
{
  point ret;

  ret.x_1 = vector.x_1 / number;
  ret.x_2 = vector.x_2 / number;
  ret.p_1 = vector.p_1 / number;
  ret.p_2 = vector.p_2 / number;

  return ret;
}
