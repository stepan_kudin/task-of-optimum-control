TEMPLATE = app
CONFIG += console
CONFIG -= qt

SOURCES += main.cpp \
    lib.cpp \
    operators.cpp

HEADERS += \
    lib.h \
    types.h \
    const.h \
    operators.h

