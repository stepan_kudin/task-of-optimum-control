#ifndef LIB_H
#define LIB_H

#include <stdio.h>
#include <math.h>
#include "types.h"
#include "operators.h"
#include "const.h"

/* Алгорим Рунге-Кутта
 * point starting_condition - начальные условия для алгоритма
 * double tau - шаг алгоритма
 * int n - количество шагов алгоритма
*/
point runge_kutta(point starting_condition, double tau, int n, bool flag_print);

point f(point u);

double norm(double x, double y);

inline double integrand(point value);
inline double simpson_rule(point point_1, point point_2, point point_3, double tau);
double integration_functions(point starting_condition, double t, int count_steps);

point newton_solver(point boundary_condition, double tau, int n);
#endif // LIB_H
