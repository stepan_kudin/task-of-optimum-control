#ifndef CONST_H
#define CONST_H

const double EPS = 1e-10;
const int MAX_STEPS = 10000;
const double DELTA = 1e-6;

#endif // CONST_H
