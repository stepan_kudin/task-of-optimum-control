#ifndef OPERATORS_H
#define OPERATORS_H
#include "types.h"

point operator +(point left, point right);

point operator *(double number, point vector);

point operator *(point vector, double number);

point operator /(point vector, double number);

#endif // OPERATORS_H
